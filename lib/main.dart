import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Custom Fonts',
    theme: ThemeData(
      fontFamily: 'DancingScript',
    ),
    home: HomePage(),
  ));
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Praktikum 2 Custom Fonts'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Ini Font DancingScript Reguler',
              style: TextStyle(color: Colors.blue, fontSize: 25, fontFamily: 'DancingScript'),
            ),
            Text(
              'Ini Font DancingScript Bold',
              style: TextStyle(color: Colors.red, fontSize: 25, fontFamily: 'DancingScriptBold'),
            ),
          ],
        ),
      ),
    );
  }
}
